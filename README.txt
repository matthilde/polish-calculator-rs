POLISH NOTATION CALCULATOR

This is a simple polish notation calculator written in Rust.
This was an exercise as I am currently learning Rust.

Available functions: + - / *

Example:
    >>> (* (+ 1 3) (- 10 6))
    16
    >>>
