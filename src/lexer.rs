use std::fmt;

pub struct Lexer<'a> {
    pos: usize,
    expr: &'a str,
}

pub enum TokenType {
    Number(i32),
    Symbol(String),
    OpenParen, ClosedParen,
}

pub struct Token(pub usize, pub TokenType);

impl fmt::Display for TokenType {
    fn fmt(&self, f:&mut fmt::Formatter) -> fmt::Result {
        use TokenType::*;
        match self {
            Number(n) => write!(f, "{n}"),
            Symbol(s) => write!(f, "{s}"),
            OpenParen => write!(f, "'('"),
            ClosedParen => write!(f, "')'")
        }
    }
}

impl fmt::Display for Token {
    fn fmt(&self, f:&mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.0, self.1)
    }
}

impl Lexer<'_> {
    pub fn new<'a>(expr: &'a str) -> Lexer<'a> {
        Lexer { pos: 0, expr }
    }

    fn current(&mut self) -> Option<char> {
        self.expr.chars().nth(self.pos)
    }
    fn next(&mut self) -> Option<char> {
        let c = self.current(); self.pos += 1; c
    }

    fn parse_number(&mut self) -> Option<Token> {
        let tokpos = self.pos;
        let mut n: i32 = 0;
        while let Some(c) = self.current() {
            if !c.is_ascii_digit() { break }

            n = n * 10 + (c.to_digit(10)? as i32);
            self.next();
        }
        Some(Token(tokpos, TokenType::Number(n)))
    }

    fn is_symbol(c: &char) -> bool {
        !(c.is_ascii_whitespace() || *c == '(' || *c == ')')
    }

    fn parse_symbol(&mut self) -> Option<Token> {
        let tokpos = self.pos;
        let mut s = String::new();
        while let Some(c) = self.current() {
            if c.is_ascii_whitespace() || c == '(' || c == ')' { break }

            s.push(self.next()?); // Should not return None.
        }
        Some(Token(tokpos, TokenType::Symbol(s)))
    }

    pub fn read_next(&mut self) -> Option<Token> {
        while self.current()?.is_ascii_whitespace() { self.next(); }

        match self.current()? {
            '(' => {
                let t = Some(Token(self.pos, TokenType::OpenParen));
                self.next();
                t
            },
            ')' => {
                let t = Some(Token(self.pos, TokenType::ClosedParen));
                self.next();
                t
            },
            '0'..='9' => self.parse_number(),
            _ => if Lexer::is_symbol(&self.current()?) {
                self.parse_symbol()
            } else { None }
        }
    }
}

impl Iterator for Lexer<'_> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        if self.pos >= self.expr.len() { return None }
        self.read_next()
    }
}
