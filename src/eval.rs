use crate::parser::AST;

fn eval_call(ast: &Vec<AST>) -> Result<i32, &'static str> {
    if ast.len() < 2 { return Err("Invalid call") }

    let fn_name = if let AST::Symbol(s) = &ast[0] {
        s
    } else { return Err("First list element must be a symbol.") };

    let mut body: Vec<i32> = vec![];
    for n in (ast[1..]).iter() { body.push(eval(n)?.clone()); }

    let iter = body.into_iter();

    match fn_name.as_str() {
        "+" => Ok(iter.reduce(|acc, n| acc + n).unwrap()),
        "-" => Ok(iter.reduce(|acc, n| acc - n).unwrap()),
        "*" => Ok(iter.reduce(|acc, n| acc * n).unwrap()),
        "/" => Ok(iter.reduce(|acc, n| acc / n).unwrap()),

        _ => Err("Function does not exist")
    }
}

pub fn eval(ast: &AST) -> Result<i32, &'static str> {
    use AST::*;
    match ast {
        Number(n) => Ok(n.clone()),
        Symbol(_) => Err("Symbol should not be here"),
        List(l)   => eval_call(l),
        _ => Err("guh???")
    }
}
