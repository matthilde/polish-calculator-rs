use std::fmt;

pub enum AST {
    Symbol(String),
    Number(i32),
    List(Vec<AST>),
    Empty,
    ClosedParen
}

impl fmt::Display for AST {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use AST::*;
        match self {
            Symbol(s) => write!(f, "{s}"),
            Number(n) => write!(f, "{n}"),
            List(l)   => {
                write!(f, "(")?;
                for node in l { write!(f, "{node} ")?; };
                write!(f, ")")
            }
            Empty    => write!(f, "nil"),
            _        => write!(f, "??"),
        }
    }
}

pub mod Parser {
    use crate::lexer::{Lexer, Token, TokenType};
    use super::AST;

    fn parse_list(lexer: &mut Lexer) -> Result<AST, &'static str> {
        use AST::*;
        let mut result: Vec<AST> = vec![];

        loop {
            let el = parse_next(lexer)?;
            match el {
                ClosedParen => break,
                Empty       => return Err("Unexpected EOF"),
                _ => result.push(el)
            }
        }
        Ok(AST::List(result))
    }

    fn parse_next(lexer: &mut Lexer) -> Result<AST, &'static str> {
        use TokenType::*;
        match lexer.next() {
            Some(Token(_, Number(n)))   => Ok(AST::Number(n)),
            Some(Token(_, Symbol(s)))   => Ok(AST::Symbol(s)),
            Some(Token(_, OpenParen))   => parse_list(lexer),
            Some(Token(_, ClosedParen)) => Ok(AST::ClosedParen),
            None => Ok(AST::Empty),
        }
    }

    pub fn parse(lexer: &mut Lexer) -> Result<AST, &'static str> {
        use AST::*;
        match parse_next(lexer)? {
            ClosedParen => Err("Unexpected closing paren"),
            ast => Ok(ast)
        }
    }
}
