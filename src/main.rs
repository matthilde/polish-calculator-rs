mod lexer;
mod parser;
mod eval;

use std::io::prelude::*;
use crate::lexer::Lexer;
use crate::parser::Parser;

fn main() {
    // REPL
    println!("Lisp calculator!");

    loop {
        let cmd = readline(">>> ");
        if let Err(e) = run(&cmd) {
            println!("ERROR! {e}");
        }
    }
}

fn readline(prefix: &str) -> String {
    use std::io;

    print!("{prefix}"); let _ = io::stdout().flush();

    let mut buf = String::new();
    io::stdin().read_line(&mut buf).unwrap();

    buf.trim().to_string()
}

fn run(cmd: &str) -> Result<(), &'static str> {
    use crate::eval::eval;
    let mut lex = Lexer::new(cmd);

    let ast = Parser::parse(&mut lex)?;
    println!("{}", eval(&ast)?);

    Ok(())
}
